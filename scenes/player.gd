extends Node2D

@onready var push_area: Area2D = $PushArea
@export var blow_force: float = 1


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	pass


func _input(event):
	if event is InputEventMouseMotion:
		var cursor_position = event.position

		set_position(cursor_position)


func _on_push_area_body_entered(body: Node2D) -> void:
	if body.is_in_group("leaf"):
		var impulse_direction := body.position - position
		body.apply_central_impulse(impulse_direction * blow_force)
